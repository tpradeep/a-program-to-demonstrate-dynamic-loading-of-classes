 // A class with name test
 public class Test
{
int a,b;
String c;
//Constructor with zero arguments
public Test()
{
System.out.println("no arg constructor called");
}
//Constructors with one single arguments
public Test(String c)
{
this.c=c;
System.out.println("1 arg constructor called");
System.out.println(c);
}
//Constructors with two arguments
public Test(int a,int b)
{
this.a=a;
this.b=b;
System.out.println("2 arg constructor called");
System.out.println(a);
System.out.println(b);
}
//constructors with three arguments
public Test(int a,int b,String c)
{
this.a=a;
this.b=b;
this.c=c;
System.out.println("3 arg constructor called");
System.out.println(a);
System.out.println(b);
System.out.println(c);
}
//Method with three parameters
public void setValues(int a,int b,String name)
{
System.out.println("3 arg method called");
this.a = a;
this.b = b;
this.c = name;
}
//Methods with two parameters
public void setValues(int a,int b)
{
this.a = a;
this.b = b;
System.out.println("2 arg method called");
}
//Methods with one parameter
public void setValues(String name)
{
System.out.println("1 arg methood called");
this.c = name;
}
public void printValueOfA()
{
System.out.println(this.a);
}
public void printValueOfB()
{
System.out.println(this.b);
}
public void printValueOfC()
{
System.out.println(this.c);
}
}
_________________________________________________________________________________________________________________________________________________
//A class with name Test2
public class Test2 {
	public int x;
	public int y;
	public int z;
	//constructors with three parameters
	public Test2(int x, int y, int z) {
		
		this.x = x;
		this.y = y;
		this.z = z;
		System.out.println("Three arguments constuctor call"+x +y +z);
	}
	//constructor with two parameters
	public Test2(int x, int y) {
		
		this.x = x;
		this.y = y;		
		System.out.println("Two arguments constuctor call" +x +y);
	}
	//constructors with one parameter
	public Test2(int x) {
		
		this.x = x;
		System.out.println("one arguments constuctor call" +x);
	}
	//method
	public void printX() {
		System.out.println(x);
	}
	//Methods with parameters
	public void setValues(int x,int y,int z) {
		this.x = x;
		this.y = y;
		this.z = z;
       System.out.println("Three arguments method call"+x +y +z);
	}
	
	public void printY() {
		System.out.println(y);
	}
	//Method with parameter
	public void setValues(int y,int z) {
		this.y = y;
		this.z = z;
       System.out.println("Two arguments method call"+ y +z);
	}
	public void printZ() {
		System.out.println(z);
	}
	public void setValues(int z) {
		this.z = z;
        System.out.println("One argument method call"+z);
	
	}
	

}
______________________________________________________________________________________________________________________________________________
//A program to Demonstrate Dynamic loading of classes(Loading of classes at runtime).
import java.lang.reflect.*;
class TestDemo
{

public static void main(String args[])
{
try
{
Class c = Class.forName(args[0]);
Class c1 = Class.forName(args[1]);
//calling Constructors
Constructor constructor =
        c.getConstructor(new Class[]{String.class});
Object ob = constructor.newInstance("pradeep");
System.out.println("The name of constructor is " + 
                            constructor.getName()); 
  
 constructor =
        c.getConstructor(new Class[]{int.class,int.class});
 ob = constructor.newInstance(10,20);
System.out.println("The name of constructor is " + 
                            constructor.getName()); 
  constructor =
        c.getConstructor(new Class[]{int.class,int.class,String.class});
 ob = constructor.newInstance(10,20,"abc");
System.out.println("The name of constructor is " + 
                            constructor.getName()); 
  

//calling Methods
Method[] methods = c.getMethods(); 
for (Method method:methods) 
            System.out.println(method.getName()); 
Method methodcall1 = c.getDeclaredMethod("setValues", 
                                                 String.class); 
methodcall1.invoke(ob, "abc"); 
  
methodcall1 = c.getDeclaredMethod("setValues", int.class,int.class,
                                                 String.class); 
methodcall1.invoke(ob, 10,20,"abc"); 

methodcall1 = c.getDeclaredMethod("setValues", 
                                                 int.class,int.class); 
methodcall1.invoke(ob,10,20);   
 
//calling Fields 
  Field[] field = c.getDeclaredFields(); 
for(Field f:field){
f.setAccessible(true);
System.out.println(f);
  }
Field f = c.getDeclaredField("a");
f.setAccessible(true);
f.set(ob, 20);
 methodcall1 = c.getDeclaredMethod("printValueOfA"); 
                                               
methodcall1.invoke(ob); 

 f = c.getDeclaredField("b");
f.setAccessible(true);
f.set(ob, 30);
 methodcall1 = c.getDeclaredMethod("printValueOfB"); 
                                               
methodcall1.invoke(ob);

f = c.getDeclaredField("c");
f.setAccessible(true);
f.set(ob,"def");
 methodcall1 = c.getDeclaredMethod("printValueOfC"); 
                                               
methodcall1.invoke(ob);


//Object ob1 = c1.newInstance();


Constructor constructor1 =
        c1.getConstructor(new Class[]{int.class});
Object ob1 = constructor1.newInstance(10);
System.out.println("The name of constructor is " + 
                            constructor1.getName()); 
  
 constructor1 =
        c1.getConstructor(new Class[]{int.class,int.class});
 ob1 = constructor1.newInstance(10,20);
System.out.println("The name of constructor is " + 
                            constructor1.getName()); 
  constructor1 =
        c1.getConstructor(new Class[]{int.class,int.class,int.class});
 ob1 = constructor1.newInstance(10,20,30);
System.out.println("The name of constructor is " + 
                            constructor1.getName()); 
  


Method[] methods1 = c1.getMethods(); 
for (Method method:methods1) 
            System.out.println(method.getName()); 
 methodcall1 = c1.getDeclaredMethod("setValues", 
                                                 int.class); 
methodcall1.invoke(ob1, 20); 
  
methodcall1 = c1.getDeclaredMethod("setValues", int.class,int.class,
                                                 int.class); 
methodcall1.invoke(ob1, 10,20,30); 

methodcall1 = c1.getDeclaredMethod("setValues", 
                                                 int.class,int.class); 
methodcall1.invoke(ob1,10,20);   
  
  Field[] field2 = c1.getDeclaredFields(); 
for(Field f3:field2){
f3.setAccessible(true);
System.out.println(f3);
  }
Field f2 = c1.getDeclaredField("x");
f2.setAccessible(true);
f2.set(ob1, 20);
 methodcall1 = c1.getDeclaredMethod("printX"); 
                                               
methodcall1.invoke(ob1); 

 f2 = c1.getDeclaredField("y");
f2.setAccessible(true);
f2.set(ob1, 30);
 methodcall1 = c1.getDeclaredMethod("printY"); 
                                               
methodcall1.invoke(ob1);

f2 = c1.getDeclaredField("z");
f2.setAccessible(true);
f2.set(ob1,40);
 methodcall1 = c1.getDeclaredMethod("printZ"); 
                                               
methodcall1.invoke(ob1);

}
catch(NoSuchFieldException e)
{

System.out.println(e);
}
catch(NoSuchMethodException e)
{

System.out.println(e);
}
catch(InvocationTargetException e)
{

System.out.println(e);
}
catch(ClassNotFoundException e)
{

System.out.println(e);

}
catch(InstantiationException e)
{

System.out.println(e);

}
catch(ArrayIndexOutOfBoundsException e)
{

System.out.println(e);

}
catch(IllegalAccessException e)
{

System.out.println(e);
}
}
}
